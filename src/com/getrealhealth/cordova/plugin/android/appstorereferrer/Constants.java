package com.getrealhealth.cordova.plugin.android.appstorereferrer;

public final class Constants {
    public static final String KEY_REFERRER = "appstoreReferrer";
    public static final String CONST_REFERRER = "referrer";
}