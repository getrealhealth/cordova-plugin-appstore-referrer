package com.getrealhealth.cordova.plugin.android.appstorereferrer;

import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String referrerString = extras.getString(Constants.CONST_REFERRER);
            if (referrerString != null) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

                Editor edit = sharedPreferences.edit();
                edit.putString(Constants.KEY_REFERRER, referrerString);
                edit.commit();
            }
        }
    }
 
}
