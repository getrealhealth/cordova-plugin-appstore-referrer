package com.getrealhealth.cordova.plugin.android.appstorereferrer;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

public class AppstoreReferrerPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getReferrer")) {
            this.getReferrer(callbackContext);
            return true;
        }

        if(action.equals("clear")){
            this.clear(callbackContext);
            return true;
        }
        return false;
    }
    
    private void clear(CallbackContext callbackContext){
        cordova.getThreadPool().execute(() -> {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(cordova.getContext());

            Editor edit = sharedPreferences.edit();
            edit.remove(Constants.KEY_REFERRER);
            edit.commit();
            callbackContext.success("clear success");
        });
    }

    private void getReferrer(CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(cordova.getContext());
            callbackContext.success(sharedPreferences.getString(Constants.KEY_REFERRER, ""));
        });
    }
}