var exec = require('cordova/exec');

let AppstoreReferrer = {};

AppstoreReferrer.getReferrer = function (success, error) {
    exec(success, error, 'AppstoreReferrerPlugin', 'getReferrer', []);
};

AppstoreReferrer.clear = function(success, error){
    exec(success, error, 'AppstoreReferrerPlugin', 'clear', []);
}

module.exports = AppstoreReferrer;