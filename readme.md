# cordova-plugin-appstore-referrer

This plugin is useful for working with app store referrer on android. This captures the refferrer value passed by the app store during installation and let the developer retriev it for later use.


There are two components to this plugin. A receiver and a data accessor.

The receiver is a [BroadcastReceiver](https://developer.android.com/reference/android/content/BroadcastReceiver) which is registered to receive the intend `com.android.vending.INSTALL_REFERRER`. When received, the data will be stored in the [SharedPreferences](https://developer.android.com/reference/android/content/SharedPreferences) under the key `appstoreReferrer`. This normally happens when the application is installed. You can however, send the intend any time to test via the `adb shell` using the following command.

    am broadcast -a com.android.vending.INSTALL_REFERRER -n io.cordova.hellocordova/com.getrealhealth.cordova.plugin.android.appstorereferrer.Receiver --es "referrer" "test_referrer=test"


The accessor can be then used to access the value later. The plugin can be accessed via `window.AppstoreReferrer`. 
There are two methods available at the moment:

* *getReferrer* - To get the value stored. Returns empty string if nothing found.
* *clear* - To clear the value stored

Both methods accepts a success and error call back. See sample usage below:

    window.AppstoreReferrer.getReferrer((value)=>{
                console.log('Got Referrer Value',value);
            }, (err)=>{
                console.error('Error getting referrer value', err);
            })
